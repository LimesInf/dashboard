module.exports = function (dataBase) {
    var express = require('express');
    var bodyParser = require('body-parser');
    var urlencoder = bodyParser.urlencoded({extended: false});
    var router = express.Router();
    var updateCount;
    var demoCollection;
    var liveNewsCollection;
    var paths;
    var idTobeDeleted;
    var updateCountCollection;
    //GET REQs
    router.get('/', function (req, res, next) {
        demoCollection = dataBase.collection('news_demo');
        liveNewsCollection=dataBase.collection('news');
        paths=dataBase.collection('imagePaths');
        demoCollection.find({}).toArray(function (err, data) {
            paths.find({}).toArray(function (err,data1) {
                res.render('index', {data: data,data1});
            });
        });
        updateCountCollection=dataBase.collection('date');
        updateCountCollection.findOne({_id:"date"},function (error,data) {
            updateCount=data.updateCount;
        })
    });

    router.get('/json', function (req, res, next) {
        demoCollection.find({}).toArray(function (err, data) {
                res.json(data);
        });
    });
    ///




    //Post reqs
    router.post('/news/document', urlencoder, function (req, res, next) {
        var newsObj = {
            name : req.body.name,
            imagePath: req.body.imagePath,

            de: {
                text: "",
                title: ""
            },
            en: {
                text: req.body.text,
                title: req.body.title
            },
            es: {
                text: "",
                title: ""
            },
            fr: {
                text: "",
                title: ""
            },
            arm: {
                text: "",
                title: ""
            },
            it: {
                text: "",
                title: ""
            },
            pt: {
                text: "",
                title: ""
            },
            ru: {
                text: "",
                title: ""
            },
            zh_CN: {
                text: "",
                title: ""
            },
            zh_TW: {
                text: "",
                title: ""
            },

        };
        demoCollection.insertOne(newsObj, function (err, data) {
            res.json(data);
        });
    });//working correctly


    router.post('/live/document',function (req,res,err) {
        demoCollection.findOne({name:req.body.nameOfClickedDocument},function (error,data) {
            liveNewsCollection.update({_id:data._id},data,{upsert:true},function (err,data) {
                res.json(data)
            })
        })

    });

    ///


    //PUT reqs
    router.put('/document',function (req ,res,err) {
        let filter={
            name : req.body.idForChange
        };
        let engTranslate={
            text:req.body.text,
            title:req.body.title,
        };
        let newDocument={$set: {name :req.body.name,imagePath:req.body.imagePath,en:engTranslate}};
        demoCollection.updateOne(filter,newDocument,function (err,data) {
            res.json(data);
        })
    });
    router.put('/news/update/count',function (req,res,err) {
          const datafilter= {
              _id:'date'
          };
          updateCount++;
        const newValue={
            $set: {updateCount:updateCount}
        };
          updateCountCollection.updateOne(datafilter,newValue,function (error,data) {
              res.json(data);
          });
    });
    ///


    ////



    //DELETE REQS
    router.delete('/news', function (req, res) {
        idTobeDeleted = req.body.name;
        demoCollection.deleteOne({name: idTobeDeleted}, function (err, data) {
            res.json(data);
        });
    });

    return router;
};


