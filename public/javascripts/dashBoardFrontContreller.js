$(document).ready(function () {
    function clear() {
        $('#nameId').val('');
        $('#titleId').val('');
        $('#newsTextId').val('');
    }

    function setView(title, text, imagePath) {
        $('#title').text(title);
        $('#text').text(text);
        $("#dataImage").attr("src", "./images/" + imagePath);
        $('#MyModel').css("display", "block");
    }
    let clickedDocumentName;
    let dbData;
    let item ;
    let newsTitle;
    let text;
    let editMode=false;
    let newOne;
    item = $('#nameId');
    newsTitle = $('#titleId');
    text = $('#newsTextId');

    $.ajax({
        type: "GET",
        url: '/json',
        success: function (data) {
            dbData = data;
        }
    });

    $('#getStarted').on('click', 'li', function () {
        clickedDocumentName = $(this).text();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $('#getStarted li').removeClass('not-selected');
            console.log("its");
            $('.aa').remove();
            clickedDocumentName = null;
            $("#editBtn").removeClass("seletedButton");
            clear();
        }
        else {
            $('#getStarted li').removeClass('selected').addClass('not-selected');
            $(this).removeClass('not-selected').addClass('selected');
            let obj = $.grep(dbData, function (obj) {
                return obj.name === clickedDocumentName;
            })[0];
            console.log(obj);
            $('#nameId').val(obj.name);
            $('#titleId').val(obj['en'].title);
            $('#newsTextId').val(obj['en'].text);
            $(clickedDocumentName).append("<div class='aa'></div>");
        }
    });

    $('#clearId').on('click', function () {
        clear();
    });


    $('#view').on('click', function () {
        if (clickedDocumentName === null) {
            let path = $('#dropdownId').find(":selected").text();
            setView(newsTitle.val(), text.val(), path)
        } else {
            let obj = $.grep(dbData, function (obj) {
                return obj.name === clickedDocumentName;
            })[0];
            let imagePath = obj.imagePath;
            setView(obj['en'].title, obj['en'].text, imagePath)

        }
    });

    $('#closeBtn').on('click', function () {
        $('#MyModel').css("display", "none");
    });

    $('form').on('submit', function () {
        let path = $('#dropdownId').find(":selected").text();
        newOne = {
            name: item.val(),
            imagePath: path,
            text: text.val(),
            title: newsTitle.val(),
            idForChange:clickedDocumentName
        };
        if (editMode) {
            $.ajax({
                type: "PUT",
                url: "/document",
                data: newOne,
                success: function () {
                    alert("updated");
                    location.reload();
                }
            });

        } else {
            $.ajax({
                type: 'POST',
                url: '/news/document',
                data: newOne,
                success: function (data) {
                    alert("Added");
                location.reload();
                }
            });

        }
        $.ajax({
            type: 'PUT',
            url: '/news/update/count',
            success: function (data) {

            }
        });

        return true;
    });

    $('#delete').on('click', function () {
        alert(clickedDocumentName);
        if (clickedDocumentName != null) {
            $.ajax({
                type: 'DELETE',
                url: '/news',
                data: {name: clickedDocumentName},
                success: function (data) {
                    location.reload();
                }
            });
        } else {
            alert("No Document Selected for Deleting,Please Select Document");
        }
        return true;
    });

    $('#editBtn').on('click', function () {
        if (clickedDocumentName != null) {
            if ($(this).hasClass("seletedButton")) {
                $(this).removeClass("seletedButton");
                $(this).addClass("non-seletedButton");
                editMode = false;
            } else {
                $(this).addClass("seletedButton");
                $(this).removeClass("non-seletedButton");
                editMode = true;
            }

        } else {
            alert("select from draft to edit")
        }
    });
    $("#publish").on('click',function () {
        if(clickedDocumentName!=null){
            $.ajax({
                type: 'POST',
                url: "/live/document",
                data: {nameOfClickedDocument:clickedDocumentName},
                success: function () {
                    alert("PUBLISHED TO LIVE");
                    location.reload()
                }
            })
        }
    })

});




